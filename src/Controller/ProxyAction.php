<?php

/*
 * This file is part of the http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker\Controller;

use Drosalys\HttpMocker\Cache\ResponseCacheManager;
use Drosalys\HttpMocker\Http\HeadersCleaner;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

/**
 * Class ProxyAction
 *
 * @author Benjamin Georgeault
 */
class ProxyAction
{
    private ResponseCacheManager $cacheManager;

    private HeadersCleaner $cleaner;

    /**
     * ProxyAction constructor.
     * @param ResponseCacheManager $cacheManager
     * @param HeadersCleaner $cleaner
     */
    public function __construct(ResponseCacheManager $cacheManager, HeadersCleaner $cleaner)
    {
        $this->cacheManager = $cacheManager;
        $this->cleaner = $cleaner;
    }

    /**
     * @param Request $request
     * @param string $proxy
     * @param string $path
     * @param bool $cachable
     * @return Response
     */
    public function __invoke(Request $request, string $proxy, string $path, bool $cachable): Response
    {
        if ($this->cacheManager->isEnabled() && $cachable) {
            if (null !== $response = $this->cacheManager->get($request)) {
                return $response;
            }
        }

        try {
            ($client = new HttpBrowser())->request(
                $request->getMethod(),
                rtrim($proxy, '/').'/'.ltrim($path, '/'),
                (0 < $request->request->count()) ? $request->request->all() : []
            );

            /** @var \Symfony\Component\BrowserKit\Response $remoteResponse */
            $remoteResponse = $client->getResponse();
            $headers = $this->cleaner->clean($remoteResponse->getHeaders());

            $response = new Response($remoteResponse->getContent(), $remoteResponse->getStatusCode(), $headers);

            if ($this->cacheManager->isEnabled() && $cachable) {
                $this->cacheManager->set($request, $response);
            }

            return $response;
        } catch (ExceptionInterface $e) {
            return new Response($e->getMessage(), 503, [
                'Content-Type' => 'text/plain',
            ]);
        }
    }
}
