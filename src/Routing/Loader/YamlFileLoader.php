<?php

/*
 * This file is part of the drosalys-web/http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Drosalys\HttpMocker\Routing\Loader;

use Drosalys\HttpMocker\Controller\FileAction;
use Drosalys\HttpMocker\Controller\ProxyAction;
use Drosalys\HttpMocker\Controller\RemoteAction;
use Symfony\Component\Routing\Loader\YamlFileLoader as BaseLoader;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class YamlFileLoader
 *
 * @author Benjamin Georgeault
 */
class YamlFileLoader extends BaseLoader
{
    /**
     * @inheritDoc
     */
    protected function parseRoute(RouteCollection $collection, string $name, array $config, string $path)
    {
        if (isset($config['defaults']['proxy'])) {
            $config['controller'] = ProxyAction::class;
            $config['path'] = rtrim($config['path'], '/').'/{path}';
            $config['requirements'] = array_merge($config['requirements'] ?? [], [
                'path' => '.*',
            ]);

            if (!isset($config['defaults']['cachable'])) {
                $config['defaults']['cachable'] = true;
            }
        }

        if (!isset($config['controller'])) {
            $config['controller'] = isset($config['defaults']['file']) ? FileAction::class : RemoteAction::class;
        }

        if (isset($config['defaults']['remote']) && !isset($config['defaults']['cachable'])) {
            $config['defaults']['cachable'] = true;
        }

        if (isset($config['defaults']['file'])) {
            $config['defaults']['file'] = $this->getRealpathFile($path, $config['defaults']['file'], $name);
        }

        parent::parseRoute($collection, $name, $config, $path);
    }

    /**
     * @inheritDoc
     */
    protected function validate($config, string $name, string $path)
    {
        parent::validate($config, $name, $path);

        if (!isset($config['controller'])) {
            if (!isset($config['defaults']['file']) && !isset($config['defaults']['remote']) && !isset($config['defaults']['proxy'])) {
                throw new \InvalidArgumentException(sprintf('You must define a "defaults.file", "defaults.remote" or  "defaults.proxy" for the route "%s" in file "%s".', $name, $path));
            }
        }

        if (isset($config['defaults']['file'])) {
            if (!is_string($config['defaults']['file'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.file" for the route "%s" in file "%s" has to be a string.', $name, $path));
            }

            if (empty($config['defaults']['file'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.file" for the route "%s" in file "%s" must not be empty.', $name, $path));
            }

            $this->getRealpathFile($path, $config['defaults']['file'], $name);
        }

        if (isset($config['defaults']['remote'])) {
            if (!is_string($config['defaults']['remote'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.remote" for the route "%s" in file "%s" has to be a string.', $name, $path));
            }

            if (!filter_var($config['defaults']['remote'], FILTER_VALIDATE_URL)) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.remote" for the route "%s" in file "%s" has to be a valid URL.', $name, $path));
            }
        }

        if (isset($config['defaults']['proxy'])) {
            if (!is_string($config['defaults']['proxy'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.proxy" for the route "%s" in file "%s" has to be a string.', $name, $path));
            }

            if (!filter_var($config['defaults']['proxy'], FILTER_VALIDATE_URL)) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.proxy" for the route "%s" in file "%s" has to be a valid URL.', $name, $path));
            }
        }

        if (isset($config['defaults']['cachable'])) {
            if (!is_bool($config['defaults']['cachable'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.cachable" for the route "%s" in file "%s" has to be a boolean.', $name, $path));
            }

            if (isset($config['defaults']['file'])) {
                throw new \InvalidArgumentException(sprintf('The parameter "defaults.cachable" for the route "%s" in file "%s" cannot be set with "defaults.file" parameter.', $name, $path));
            }
        }
    }

    /**
     * @param string $path
     * @param string $file
     * @param string $name
     * @return string
     */
    private function getRealpathFile(string $path, string $file, string $name): string
    {
        if (false === $filePath = realpath(dirname($path).'/'.$file)) {
            throw new \InvalidArgumentException(sprintf(
                'The file "%s" given by the parameter "defaults.file" for the route "%s" in file "%s" does not exist.',
                $file,
                $name,
                $path
            ));
        }

        return $filePath;
    }
}
