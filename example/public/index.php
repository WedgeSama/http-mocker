<?php
/*
 * This file is part of the drosalys-web/http-mocker package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Drosalys\HttpMocker\HttpMocker;

require_once __DIR__.'/../../vendor/autoload.php';

$mocker = new HttpMocker(__DIR__.'/..', __DIR__.'/../cache');

$mocker();
